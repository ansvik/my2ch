<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>My 2ch</title>
		<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="/css/style.css">

	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="/Main/index">My2ch</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 			aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			  <div class="collapse navbar-collapse" id="navbarNavDropdown">
			    <ul class="navbar-nav">
			      <?php
			      foreach ($data["default"] as $key => $value) {
			      	echo "<li class=\"nav-item\">";
			      	echo "<a class=\"nav-link\" href=\"/Threads/" . $value["id"] ."\">" . $value["name"] ."</a>";
			      	echo "</li>";
			      }
			      ?>
			    </ul>
			</div>
		</nav>
		<div class="container mt-3">
			<div class="row">
				<div class="col-sm">
					<?php include 'application/views/'.$content_view; ?>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="/js/jquery-1.6.2.js"></script>
		<script type="text/javascript" src="/js/bootstrap.js"></script>
	</body>
</html>