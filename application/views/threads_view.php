<div class="text-right">
	<button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Добавить</button>
</div>
<?php 

for ($i = $data["count"] - 1; $i >= 0; $i--) {
	echo "<div class='post text-white bg-dark'>";
	echo "<div class='row'>";
	if (isset($data[$i]["img_link"]) && $data[$i]["img_link"] != "") {
		echo "<div class='col-3'>";
		echo "<img src='" . $data[$i]["img_link"] . "' class='img'><br>";
		echo "</div>";
		echo "<div class='col'>";
	}
	else 
	{
		echo "<div class='col'>";
	}
	echo "<h5>" . $data[$i]["name"] . "</h4>";
	echo "<p>" . $data[$i]["main_text"] . "</p>";
	echo "</div>";
	echo "</div>";
	echo "<hr class='hr'>";
	echo "<div class='text-right mr-4'>";
	echo "<a href='/Thread/" . $data[$i]["id"] . "' class='text-primary'>Comments</a>";
	echo "</div>";
	echo "</div>";
}

?>
<div class='float-right mr-4'>
	<nav>
	  <ul class="pagination">
	    <?php
	    	if ($data["page"] != 1) {
	    		echo "<li class=\"page-item\"><a class=\"page-link\" href=\"/Threads/" . $data["category"] . "?" . ($data["page"] - 2) . "\">Previous</a></li>";
	    	}
	    	for ($i = 1; $i <= ceil($data["countpag"]); $i++)
			{
				echo "<li class=\"page-item\"><a class=\"page-link\" href=\"/Threads/" . $data["category"] . "?" . ($i - 1) . "\">" . $i ."</a></li>";
			}
			if ($data["page"] != ceil($data["countpag"]) && ceil($data["countpag"]) != 0) {
	    		echo "<li class=\"page-item\"><a class=\"page-link\" href=\"/Threads/" . $data["category"] . "?" . ($data["page"]) . "\">Next</a></li>";
	    	}
	    ?>
	  </ul>
	</nav>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Добавить тред</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form enctype="multipart/form-data" action="/Thread/add_thread" method="post">
      	<div class="modal-body">
				<div class="row">
					<div class="col">
						<label>Название</label><br>
						<input type="text" name="name"><br>
						<label>Текст</label>
						<textarea name="main_text" class="form-control mb-2" rows="5"></textarea>
						<input type="hidden" name="category" value="<?php echo(explode("?", explode('/', $_SERVER['REQUEST_URI'])[2])[0]);	 ?>">
					</div>
				</div>
      	</div>
      	<div class="modal-footer">
      	  <button type="submit" class="btn btn-primary">Добавить</button>
      	  <input name="userfile" type="file" />
      	</div>
      </form>
    </div>
  </div>
</div>