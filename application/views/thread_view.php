<?php 
	echo "<div class='post-full text-white bg-dark mb-4'>";
	echo "<div class='row'>";
	if (isset($data[0]["img_link"]) && $data[0]["img_link"] != "") {
		echo "<div class='col-4'>";
		echo "<a href='" . $data[0]["img_link"] . "'>";
		echo "<img src='" . $data[0]["img_link"] . "' class='img'><br>";
		echo "</a>";
		echo "</div>";
		echo "<div class='col'>";
	}
	else {
		echo "<div class='col'>";
	}
	echo "<h5>" . $data[0]["name"] . "</h4>";
	echo "<p>" . $data[0]["main_text"] . "</p>";
	echo "</div>";
	echo "</div>";
	echo "</div>";

	echo "<hr>";
	if (isset($data[0]["comments"]))
	foreach ($data[0]["comments"] as $value) {
		echo "<div class='comment text-white bg-dark'>";
		echo "<h4>#" . $value["user"] . "</h4>";
		echo "<div class='row'>";
		if (isset($value["img_link"]) && trim($value["img_link"]) != "") {
			echo "<div class='col-2'>";
			echo "<img src='" . $value["img_link"] . "' class='img'><br>";
			echo "</div>";
			echo "<div class='col'>";
		}
		else {
			echo "<div class='col'>";
		}
		echo "<p>" . $value["main_text"] . "</p>";
		echo "</div>";
		echo "</div>";
		echo "</div>";
	}
?>

<form enctype="multipart/form-data" action="/Thread/add_comment" method="post">
	<div class="row">
		<div class="col-8">
			<label>Добавить комментарий:</label>
			<textarea name="main_text" class="form-control ta-comm  mb-2" rows="5"></textarea>
			<input type="hidden" name="thread_id" value="<?php echo(explode("?", explode('/', $_SERVER['REQUEST_URI'])[2])[0]); ?>">
			<input class="form-control mb-4" name="userfile" type="file" />
			<button type="submit" class="btn btn-primary mb-4">Добавить</button>
		</div>
	</div>
</form>

<?php echo "</div>"; ?>
