<?php

class Controller_404 extends Controller
{
	
	function action_index()
	{
		$data["default"] = $this->defmodel->get_data();
		$this->view->generate('404_view.php', 'template_view.php', $data);
	}
}
