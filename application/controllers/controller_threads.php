<?php

class Controller_Threads extends Controller
{

	function __construct()
	{
		$this->defmodel = new Model_Default();
		$this->model = new Model_Threads();
		$this->view = new View();
	}

	function action_index()
	{
		$data = $this->model->get_data();
		$data["default"] = $this->defmodel->get_data();
		if ($data["count"] == 0 && $data["page"] != 1) $this->view->generate('404_view.php', 'template_view.php', $data);
		else $this->view->generate('threads_view.php', 'template_view.php', $data);
	}
}