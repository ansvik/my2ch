<?php

class Controller_Thread extends Controller
{

	function __construct()
	{
		$this->defmodel = new Model_Default();
		$this->model = new Model_Thread();
		$this->view = new View();
	}

	function action_index()
	{	
		$data = $this->model->get_data();
		$data["default"] = $this->defmodel->get_data();
		$this->view->generate('thread_view.php', 'template_view.php', $data);
	}

	function action_add_comment() 
	{
		$uploaddir = 'images/';
		$uploadfile = $uploaddir . abs(crc32(uniqid())) . "." . pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
		if (!isset($_POST["thread_id"]) || !isset($_POST["main_text"])) 
		{
			$this->view->generate('404_view.php', 'template_view.php', $data);
			return;
		}
		if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
			$this->model->add_comment($_POST["main_text"], $_POST["thread_id"], "/" . $uploadfile);
		}
		else 
		{
			$this->model->add_comment($_POST["main_text"], $_POST["thread_id"]);
		}
		header("Location: /Thread/" . $_POST["thread_id"]);
	}

	function action_add_thread() {
		$uploaddir = 'images/';
		$uploadfile = $uploaddir . abs(crc32(uniqid())) . "." . pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);

		if (!isset($_POST["name"]) || !isset($_POST["main_text"]) || !isset($_POST["category"])) 
		{
			$this->view->generate('404_view.php', 'template_view.php', $data);
			return;
		}

		if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile))
		{
			$this->model->add_thread($_POST["name"], $_POST["main_text"], $_POST["category"], "/" . $uploadfile);
		}
		else 
		{
			$this->model->add_thread($_POST["name"], $_POST["main_text"], $_POST["category"]);
		}
		
		header("Location: /Threads/" . $_POST["category"]);
	}
}