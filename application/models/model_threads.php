<?php

class Model_Threads extends Model
{
	
	public function get_data()
	{	
		$category = explode('/', $_SERVER['REQUEST_URI'])[2];
		$page = 0;
		if (isset(explode('?', $category)[1])) {
			$page = explode('?', $category)[1];	
		}
		$category = explode('?', $category)[0];

		try {
			$page = $page * 1;
		}
		catch (Exception $e) {
			$page = 0;
		}

		$mysqli = new mysqli('localhost', 'root', '', '2ch');
		if ($mysqli->connect_error) {
    		die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
		}

		$resultAssoc = [];

		$result = $mysqli->query("SELECT count(*) FROM `threads` WHERE category_id = " . $category);
		
		if ($mysqli->errno) {
			die('Select Error (' . $mysqli->errno . ') ' . $mysqli->error);
		}
		
		$cou = $result->fetch_assoc()["count(*)"];

		$aaa = ($cou - ($page * 10) - 10);
		if ($aaa < 0) $aaa = 0;

		$result = $mysqli->query("SELECT * FROM `threads` WHERE category_id = '" . $category . "' LIMIT " . $aaa . ', ' . ($cou - ($page * 10)));

		if ($mysqli->errno) {
			die('Select Error (' . $mysqli->errno . ') ' . $mysqli->error);
		}

		for ($i = 0; $i < $result->num_rows; $i++) { 
			$resultAssoc[$i] = $result->fetch_assoc();
		}

		$resultAssoc["countpag"] = ($cou / 10);
		$resultAssoc["category"] = $category;
		$resultAssoc["page"] = $page + 1;
		$resultAssoc["count"] = $result->num_rows;

		mysqli_free_result($result);
		mysqli_close($mysqli);
		return $resultAssoc;
	}
}