<?php

class Model_Thread extends Model
{
	
	public function get_data()
	{	
		$thread = explode('/', $_SERVER['REQUEST_URI'])[2];
		$thread = explode('?', $thread)[0];

		$mysqli = new mysqli('localhost', 'root', '', '2ch');
		if ($mysqli->connect_error) {
    		die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
		}
		$result = $mysqli->query("SELECT * FROM `threads` WHERE id = " . $thread);
		
		if ($mysqli->errno) {
			die('Select Error (' . $mysqli->errno . ') ' . $mysqli->error);
		}
		$resultAssoc = [];

		$resultAssoc[0] = $result->fetch_assoc();

		if ($mysqli->connect_error) {
    		die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
		}
		$result = $mysqli->query("SELECT * FROM `comments` WHERE thread_id = " . $thread);
		
		if ($mysqli->errno) {
			die('Select Error (' . $mysqli->errno . ') ' . $mysqli->error);
		}

		for ($i = 0; $i < $result->num_rows; $i++) { 
			$resultAssoc[0]["comments"][$i] = $result->fetch_assoc();
		}

		mysqli_free_result($result);
		mysqli_close($mysqli);
		return $resultAssoc;
	}

	public function add_comment($main_text, $thread_id, $img_link = "")
	{
		if ($main_text == "") {
			echo "<label class='text-danger'>Комментарий не может быть пустым<label>";
			return;
		}

		$mysqli = new mysqli('localhost', 'root', '', '2ch');
		if ($mysqli->connect_error) {
    		die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
		}
		$mysqli->query("INSERT INTO `comments` (`id`, `thread_id`, `main_text`, `img_link`, `user`) VALUES (NULL, '" . $thread_id . "', '" . $main_text . "', '" . $img_link . "', '" . abs(crc32(uniqid())) ."')");
		
		if ($mysqli->errno) {
			die('Select Error (' . $mysqli->errno . ') ' . $mysqli->error);
		}

		mysqli_close($mysqli);
	}

	public function add_thread($name, $main_text, $category, $img_link = "")
	{
		if (trim($main_text) == "" || trim($name) == "") {
			echo "<label class='text-danger'>Комментарий не может быть пустым<label>";
			return;
		}
		$mysqli = new mysqli('localhost', 'root', '', '2ch');
		if ($mysqli->connect_error) {
    		die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
		}
		$mysqli->query("INSERT INTO `threads` (`id`, `category_id`, `name`, `main_text`, `img_link`, `user`) VALUES (NULL, '" . $category . "', '" . $name ."', '" . $main_text . "', '" . $img_link . "', " . abs(crc32(uniqid())) .")");
		
		if ($mysqli->errno) {
			die('Select Error (' . $mysqli->errno . ') ' . $mysqli->error);
		}

		mysqli_close($mysqli);
	}

}