<?php

class Model_Default extends Model
{
	
	public function get_data()
	{	
		$mysqli = new mysqli('localhost', 'root', '', '2ch');
		if ($mysqli->connect_error) {
    		die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
		}
		$result = $mysqli->query("SELECT id,name FROM `categories`");
		if ($mysqli->errno) {
			die('Select Error (' . $mysqli->errno . ') ' . $mysqli->error);
		}
		$resultAssoc = [];

		for ($i = 0; $i < $result->num_rows; $i++) { 
			$resultAssoc[$i] = $result->fetch_assoc();
		}

		mysqli_free_result($result);
		mysqli_close($mysqli);
		return  $resultAssoc;
	}

}
